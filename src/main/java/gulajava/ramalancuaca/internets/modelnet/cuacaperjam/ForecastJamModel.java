package gulajava.ramalancuaca.internets.modelnet.cuacaperjam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gulajava Ministudio.
 */
public class ForecastJamModel {

    private long dt;
    private String dt_txt;

    private MainForecastModel main;

    private List<WeatherItem> weather = new ArrayList<>();

    private CloudModel clouds;

    private WindModel wind;

    public ForecastJamModel() {
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public MainForecastModel getMain() {
        return main;
    }

    public void setMain(MainForecastModel main) {
        this.main = main;
    }

    public List<WeatherItem> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherItem> weather) {
        this.weather = weather;
    }

    public CloudModel getClouds() {
        return clouds;
    }

    public void setClouds(CloudModel clouds) {
        this.clouds = clouds;
    }

    public WindModel getWind() {
        return wind;
    }

    public void setWind(WindModel wind) {
        this.wind = wind;
    }

}
