package gulajava.ramalancuaca.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.adapters.RecyclerCuacaPerJam;
import gulajava.ramalancuaca.aktivitas.AktHalamanUtama;
import gulajava.ramalancuaca.database.RMCuacaPerJam;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import gulajava.ramalancuaca.dialogs.DialogDetilCuacaPerJam;
import gulajava.ramalancuaca.utilans.Konstan;
import gulajava.ramalancuaca.utilans.UtilanCuaca;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Gulajava Ministudio
 */
public class FragmentCuacaJam extends Fragment {


    @Bind(R.id.recycler_list)
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;

    //data
    private RecyclerCuacaPerJam mRecyclerCuacaPerJam;

    //realm
    private Realm mRealm;
    private RealmQuery<RMCuacaPerJam> mCuacaPerJamRealmQuery;
    private RealmResults<RMCuacaPerJam> mCuacaPerJamRealmResults;
    private RealmChangeListener mRealmChangeListener;
    private AktHalamanUtama mAktHalamanUtama;


    //ALTERNATIF MENERIMA PESAN BROADCAST DARI SERVIS
    private BroadcastReceiver mBroadcastReceiverServis = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String TIPE_AKSI = intent.getAction();

            if (TIPE_AKSI.contentEquals(Konstan.KODE_BROADCAST_SERVIS)) {

                //segarkan realm results
                //jika fragment sudah jalan dan sudah disegarkan
                //dari inisialisasi
                Log.w("PESAN DARI SERVIS", "FRAGMENT CUACA PER JAM MENERIMA PESAN DARI SERVIS");

            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = LayoutInflater.from(FragmentCuacaJam.this.getActivity()).inflate(R.layout.frag_cuaca_jam, container, false);
        ButterKnife.bind(FragmentCuacaJam.this, view);

        mAktHalamanUtama = (AktHalamanUtama) FragmentCuacaJam.this.getActivity();

        RealmConfiguration realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(FragmentCuacaJam.this.getActivity());
        mRealm = Realm.getInstance(realmConfiguration);

        inisialisasiRealmListener();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inisialisasiRealmResults();

    }

    @Override
    public void onResume() {
        super.onResume();
        FragmentCuacaJam.this.getActivity().registerReceiver(mBroadcastReceiverServis,
                new IntentFilter(Konstan.KODE_BROADCAST_SERVIS));
    }

    @Override
    public void onPause() {
        super.onPause();
        FragmentCuacaJam.this.getActivity().unregisterReceiver(mBroadcastReceiverServis);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(FragmentCuacaJam.this);

        mCuacaPerJamRealmResults.removeChangeListeners();
        mRealm.close();
    }


    //INISIALISASI REALM
    private void inisialisasiRealmResults() {

        mCuacaPerJamRealmQuery = mRealm.where(RMCuacaPerJam.class);
        mCuacaPerJamRealmResults = mCuacaPerJamRealmQuery.findAllAsync();
        mCuacaPerJamRealmResults.addChangeListener(mRealmChangeListener);

        mLinearLayoutManager = new LinearLayoutManager(FragmentCuacaJam.this.getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerCuacaPerJam = new RecyclerCuacaPerJam(FragmentCuacaJam.this.getActivity(), mCuacaPerJamRealmResults);
        mRecyclerCuacaPerJam.setOnItemClickListener(mOnItemClickListener);
        mRecyclerView.setAdapter(mRecyclerCuacaPerJam);

    }


    //INISIALISASI REALM LISTENER
    private void inisialisasiRealmListener() {

        mRealmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange() {

                mRealm.refresh();

                if (mCuacaPerJamRealmResults != null && mCuacaPerJamRealmResults.isLoaded()) {

                    //setel dan beritahu adapter ada data yang berubah
                    mRecyclerCuacaPerJam.segarkanDataRealm();

                    //coba segarkan gambar backdrop
                    setelLatarBelakang();
                }
            }
        };
    }


    //LISTENER RECYCLER VIEW
    RecyclerCuacaPerJam.OnItemClickListener mOnItemClickListener = new RecyclerCuacaPerJam.OnItemClickListener() {
        @Override
        public void onItemClick(int posisiKlik, int idDatabase) {

            DialogDetilCuacaPerJam dialogDetilCuacaPerJam = new DialogDetilCuacaPerJam();
            Bundle bundle = new Bundle();
            bundle.putInt(Konstan.INTENT_BARISDB, idDatabase);
            dialogDetilCuacaPerJam.setArguments(bundle);

            FragmentTransaction fragmentTransaction = FragmentCuacaJam.this.getFragmentManager().beginTransaction();
            dialogDetilCuacaPerJam.show(fragmentTransaction, "DETIL CUACA PER JAM");
        }
    };


    //SETEL GAMBAR LATAR UNTUK KONDISI HUJAN ATAU CERAH
    private void setelLatarBelakang() {

        if (mCuacaPerJamRealmResults != null && !mCuacaPerJamRealmResults.isEmpty()) {

            RMCuacaPerJam rmCuacaPerJam = mCuacaPerJamRealmResults.first();
            String kodecuaca = rmCuacaPerJam.getIdKodeCuaca();
            int kodegambar = UtilanCuaca.getStatusCuacaBackdrop(kodecuaca);

            mAktHalamanUtama.inisialisasiTampilanBackdrop(kodegambar);
        }
    }

}
